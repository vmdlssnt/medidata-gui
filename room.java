/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author FUEGO
 */
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JFrame;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.BorderFactory;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;


public class room extends JPanel{
    //JPanel p1;
    JRadioButton occupied;
    JRadioButton vacant;
    ButtonGroup bg1;
    JLabel priorityicon;
    JLabel room_number;
    Connection myConn;
    int ocupationstatus;
    int roomnumber;
    int prioritystatus;
    
    public room(int room, int vacancy, Connection conn,boolean roomsexist, int mypriority)
    {
        /*
        priority =-1 INPROGRESS
        priority =0 NOPRIORITY
        priority =1 VERYLOW
        priority =2 LOW
        priority =3 NORMAL
        priority =4 HIGH
        priority =5 VERYHIGH
        
        */
        ocupationstatus=vacancy;
        roomnumber=room;
        myConn = conn;
        prioritystatus = mypriority;
        
        setPreferredSize(new Dimension(150,150));
        //p1 = new JPanel(new BorderLayout());
        setLayout(new BorderLayout());
        //setOpaque(true);
        setBackground(Color.GREEN);
        setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY,3,true));
        JPanel subp1 = new JPanel(new FlowLayout());
        
        occupied = new JRadioButton("Occupied");
        vacant = new JRadioButton("Vacant");
        
        if(roomsexist){
            if(ocupationstatus==1)
            {
                vacant = new JRadioButton("Vacant", false);
                occupied = new JRadioButton("Ocupied",true);
            }
            else
            {
                occupied = new JRadioButton("Occupied", false);
                vacant = new JRadioButton("Vacant", true);
            }
        }else{
                vacant = new JRadioButton("Vacant", true);
                occupied = new JRadioButton("Ocupied",false);
            }
        
        
        
        occupied.setBackground(Color.LIGHT_GRAY);
        vacant.setBackground(Color.LIGHT_GRAY);
        bg1 = new ButtonGroup();
        room_number = new JLabel("Room: " + room);
                
        bg1.add(occupied);
        bg1.add(vacant);
        subp1.setBackground(Color.LIGHT_GRAY);
        
        
        subp1.add(occupied);
        subp1.add(vacant);
        
        
        
        Image verylow = new ImageIcon(getClass().getResource("verylow.png")).getImage();
        Image low = new ImageIcon(getClass().getResource("low.png")).getImage();
        Image normal = new ImageIcon(getClass().getResource("normal.png")).getImage();
        Image high = new ImageIcon(getClass().getResource("high.png")).getImage();
        Image veryhigh = new ImageIcon(getClass().getResource("veryhigh.png")).getImage();
        Image nopriority = new ImageIcon(getClass().getResource("nopriority.png")).getImage();
        Image inprogress = new ImageIcon(getClass().getResource("inprogress.png")).getImage();
        
        priorityicon = new JLabel();
        
        if (prioritystatus==0){
            priorityicon.setIcon(new ImageIcon(nopriority));
        }else if(prioritystatus==1){
            priorityicon.setIcon(new ImageIcon(verylow));
        }else if(prioritystatus==2){
            priorityicon.setIcon(new ImageIcon(low));
        }else if(prioritystatus==3){
            priorityicon.setIcon(new ImageIcon(normal));
        }else if(prioritystatus==4){
            priorityicon.setIcon(new ImageIcon(high));
        }else if(prioritystatus==5){
            priorityicon.setIcon(new ImageIcon(veryhigh));}
        else{
            priorityicon.setIcon(new ImageIcon(inprogress));
        }
        
        
        //priorityicon.setIcon(new ImageIcon(verylow));
        add(room_number, BorderLayout.NORTH);
        add(subp1, BorderLayout.CENTER);
        add(priorityicon, BorderLayout.SOUTH);
        
        if(roomsexist){
  
        }else{
            
            try{
            PreparedStatement pstmt = myConn.prepareStatement("insert into rooms (room_number,occupied) values(?,?)");


            pstmt.setInt(1, roomnumber);

            pstmt.setInt(2, 0);

            pstmt.executeUpdate();
            }catch(Exception e){System.out.println("Error while inserting new room into rooms table");}
            
        }
        
        
                occupied.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                                try{
                                    
                                    
                                    PreparedStatement pstmt = myConn.prepareStatement("UPDATE rooms set occupied = ? where room_number = ? ");
                                    pstmt.setInt(1, 1);
                                    pstmt.setInt(2, roomnumber);
                                    pstmt.executeUpdate();
                                    ocupationstatus=1;
        
        
                                }catch(Exception exc){System.out.println("Error while udpating availability status of room");}
                            	
			}
		});
                
                vacant.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                                try{
                                    
                                    
                                    PreparedStatement pstmt = myConn.prepareStatement("UPDATE rooms set occupied = ? where room_number = ? ");
                                    pstmt.setInt(1, 0);
                                    pstmt.setInt(2, roomnumber);
                                    pstmt.executeUpdate();
                                    ocupationstatus=0;
        
                                }catch(Exception exc){System.out.println("Error while udpating availability status of room");}
                            	
			}
		});
        
    }
    

                
                

}

