/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//GUI libraries
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.lang.String;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
 
//time libraries
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.util.Calendar;

//concurrency libraries
import static java.util.concurrent.TimeUnit.*;
import java.util.concurrent.*;
import java.lang.Runnable;

//MySQL JDBC connector driver library
import java.sql.*;


//lists
import java.util.LinkedList;
import java.util.Queue;
import java.util.ListIterator;
//scheduler


/**
 *
 * @author Victor De Los Santos
 */


public class gui{

	private String username;
	private	String password;
	private boolean authenticated=false;
	private JFrame myframe = new JFrame();	
	private JPanel panel1 = new JPanel();
        private boolean stopautoupdate = false;
        private boolean stopautoupdate2 = false;
        private Object bph;
        private Object bph2;
        final ScheduledExecutorService scheduler =  Executors.newScheduledThreadPool(1);
        int defaultperiodicupdate= 10; //refresh every 10 secs
        final ScheduledExecutorService scheduler2 =  Executors.newScheduledThreadPool(1);
        private int currenttabbedpane=0;
        
        

	Connection myConn;
	Statement myStmt;
	ResultSet myRs;

	public gui(){
		myframe.setTitle("Medi-Data");
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setSize(400,300);
		myframe.setVisible(true);
		((java.awt.Frame)myframe).setResizable(false);
		myframe.setLocationRelativeTo(null);

	//	JFrame myframe = new JFrame();
//		JPanel panel1 = new JPanel();
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER,50,40);
		panel1.setLayout(layout);

		Box myBox = Box.createVerticalBox();

		JLabel label1 = new JLabel("MediData");
		label1.setFont(new Font("Serif", Font.ITALIC,50));

		JLabel label2 = new JLabel("User Name:");
		JLabel label3 = new JLabel("Password:");

		final JTextField field1 = new JTextField(6);
		final JPasswordField field2 = new JPasswordField(6);

		JButton button1 = new JButton("Log in");
	

		myframe.add(panel1);

		panel1.add(myBox);

		myBox.add(label1);
		myBox.add(Box.createVerticalStrut(10));
		myBox.add(label2);
		myBox.add(Box.createVerticalStrut(10));
		myBox.add(field1);
		myBox.add(Box.createVerticalStrut(10));
		myBox.add(label3);
		myBox.add(Box.createVerticalStrut(10));
		myBox.add(field2);
		myBox.add(Box.createVerticalStrut(15));
		myBox.add(button1);

		myframe.revalidate();
		myframe.repaint();
                
                
		button1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				username = field1.getText();

				password = String.valueOf(field2.getPassword());
//				System.out.println(username);
//				System.out.println(password);

				try {
//				
				
                                
                                myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Event_Log",username ,password);
                                

                                
                                
                                menu();

				}catch (Exception exc){System.out.println("Access Denied");}

			}
		});
	}
	
	
	public void menu(){
                
		myframe.setSize(500,400);
		myframe.remove(panel1);
		panel1 = new JPanel();
		myframe.add(panel1);



		Box myBox = Box.createVerticalBox();

		JButton button1 = new JButton("View Logs");
		JButton button2 = new JButton("Exit");
                JButton button3 = new JButton("Dashboard");
                JButton button4 = new JButton("Query");
                JButton button5 = new JButton("Settings");
                /*
                button3.addActionListener(new ActionListener()
                {
                public void actionPerformed(ActionEvent e)
                    {
                           dashboard();
                    }
                });
                    */
                button2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	System.exit(0);
			}
		});
                button5.addActionListener(new ActionListener()
                {
                public void actionPerformed(ActionEvent e)
                    {
                           settings();
                    }
                });
		panel1.add(myBox);

		myBox.add(Box.createVerticalStrut(30));
		myBox.add(button1);
                myBox.add(Box.createVerticalStrut(20));
		myBox.add(button3);
                myBox.add(Box.createVerticalStrut(20));
		myBox.add(button4);
                myBox.add(Box.createVerticalStrut(20));
		myBox.add(button5);
		myBox.add(Box.createVerticalStrut(120));
		myBox.add(button2);


		myframe.revalidate();
		myframe.repaint();
		

		button1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	bph = schedulerTasks();
                                //scheduler.schedule(new Runnable() {public void run() {beeperHandle.cancel(stopautoupdate); }},100, MILLISECONDS);
				
			}
		});
                
                button3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	bph2 = schedulerTasks2();
                                //scheduler.schedule(new Runnable() {public void run() {beeperHandle.cancel(stopautoupdate); }},100, MILLISECONDS);
				
			}
		});

		
	}
        
        public Object schedulerTasks(){
            
            //final ScheduledExecutorService scheduler2 =  Executors.newScheduledThreadPool(1);
            final Runnable beeper = new Runnable() {public void run() {viewlogs(); }};
            final ScheduledFuture<?> beeperHandle = scheduler.scheduleAtFixedRate(beeper, 0, defaultperiodicupdate, SECONDS);
            return beeperHandle;
        }
        
        public Object schedulerTasks2(){
            
            //final ScheduledExecutorService scheduler2 =  Executors.newScheduledThreadPool(1);
            final Runnable beeper = new Runnable() {public void run() {dashboard(); }};
            final ScheduledFuture<?> beeperHandle = scheduler2.scheduleAtFixedRate(beeper, 0, defaultperiodicupdate, SECONDS);
            return beeperHandle;
        }
        
        public void dashboard(){
            myframe.setSize(1000,550);
            myframe.remove(panel1);
            panel1 = new JPanel(new BorderLayout(5,5));
            int prioritycolumn=6;//cannot be the first column
            JPanel floor1 = new JPanel(new FlowLayout());
            JPanel floor2 = new JPanel(new FlowLayout());
            JPanel backbuttonarea = new JPanel (new FlowLayout(FlowLayout.LEFT));
            
            JButton backbutton = new JButton("Back");
            
            JTabbedPane floors = new JTabbedPane();

                        
            backbutton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                                stopautoupdate2 = true;
                                scheduler2.schedule(new Runnable() {public void run() { ((ScheduledFuture<?>)(bph2)).cancel(true); }},0, SECONDS);
                                menu();
                        }
		});
            backbuttonarea.add(backbutton);
            
            
            boolean roomsexist = false;
            try{
                myStmt = myConn.createStatement();
                myRs =  myStmt.executeQuery("select * from rooms");
                while(myRs.next()){
                    roomsexist=true;
                }
            }catch(Exception e){System.out.println("Error while determining if rooms exist");}
            System.out.println(roomsexist);
            
            
            LinkedList<int[]> listofrooms = new LinkedList<int[]>();
            LinkedList<floorunit> listoffloors = new LinkedList<floorunit>();
            LinkedList<Integer> numberoffloors = new LinkedList<Integer>();
            
            
            
            if (roomsexist)
                {
                try{
                    myStmt = myConn.createStatement();
                    myRs =  myStmt.executeQuery("select * from rooms");
                    boolean firstime =true;
                    int firstfloordivisible=1;
                    while(myRs.next()){
                        int i =0;
                        int [] array1 = new int[2];
                        while(i<2){
                        array1[i]=(int)(myRs.getObject(i+1));
                        
                            if(i==0){
                                    int floordivisible=array1[0]/100;
                                if(firstime){
                                    firstfloordivisible = array1[0]/100;
                                    numberoffloors.add(firstfloordivisible);
                                    firstime=false;
                                }
                                if(floordivisible!=firstfloordivisible)
                                {
                                    numberoffloors.add(floordivisible);
                                    firstfloordivisible = floordivisible;
                                }
                            }
                        i++;
                        }
                        listofrooms.add(array1);
                        //System.out.println((listofrooms.peek()));
                    }
                    
                }catch(Exception e){System.out.println("Error while determining what rooms are to be used");}
                ListIterator iterfloorints = numberoffloors.listIterator();
                while(iterfloorints.hasNext()){
                    //System.out.println("step");
                    //System.out.println(iterfloorints.next());
                    JPanel floorpanel = new JPanel(new FlowLayout());
                    int floornumber = (int)(iterfloorints.next());
                    floorunit myfloorunit = new floorunit(floornumber,floorpanel);
                    listoffloors.add(myfloorunit);
                    floors.add("Floor " + floornumber, floorpanel);
                }
                
                ListIterator iter1 = listofrooms.listIterator();
                while(iter1.hasNext()) {
                    int [] array2 = ((int[])(iter1.next()));
                    room r=null;// = new room(array2[0],array2[1],myConn,roomsexist,0);
                    boolean roomtobemade = false;
                    try{
                        Statement stmt3 = myConn.createStatement();
                        ResultSet myRs2 = stmt3.executeQuery("select * from(select *,time_to_sec(timediff(now(),time_left)) as priority from (Select * from events order by time_left desc)as q1 group by room_number) as q2 left outer join rooms on q2.room_number = rooms.room_number");// where time_left is not null");
                        
                            while(myRs2.next()){
                            //System.out.println(array2[0]);
                            //System.out.println(myRs2.getObject(3));
                            //System.out.println(myRs2.getInt(6));
                            //System.out.println(myRs2.getObject(3));
                            //System.out.println("step1");
                                if(array2[0]==(int)(myRs2.getObject(3)) && myRs2.getInt(8)==1){
                                    //System.out.println("step2");
                                    //System.out.println(myRs2.getInt(6));
                                    //System.out.println(priorityinsecs);
                                    //System.out.println(determinepriority(500));
                                    //System.out.println("determine"+ determinepriority(myRs2.getInt(6)));
                                r = new room(array2[0],array2[1],myConn,roomsexist,determinepriority(myRs2.getInt(6)));
                                roomtobemade=false;
                                break;
                                }

                                else{
                                roomtobemade=true;
                                }
                            } 
                            if(roomtobemade){
                                //System.out.println("roomtobemade with prior0"+ array2[0]);
                                r = new room(array2[0],array2[1],myConn,roomsexist,0);
                            //System.out.println(roomtobemade);
                            }
                                
                            
                            
                        }catch(Exception exc){System.out.println("Error while assigning priority");}
                    
                
                    //room r = new room(array2[0],array2[1],myConn,roomsexist,0);
                    ListIterator iterfloorunits = listoffloors.listIterator();
                    while(iterfloorunits.hasNext()){
                        floorunit newfloorunit = (floorunit)(iterfloorunits.next());
                        int designatedfloor = newfloorunit.floornumber;
                        //System.out.println(designatedfloor);
                        JPanel designatedfloorpanel = newfloorunit.floorpanel;
                        if(array2[0]/100==designatedfloor)
                        {
                            designatedfloorpanel.add(r);
                        }
                    }
                    //floor1.add(r);
                }
            }else
                {
                    
                   //int i=0;
                   int roomcounter = 101; 
                    while(roomcounter<111){
                        room r = new room(roomcounter,0,myConn,roomsexist,0);
                        roomcounter++;
                        floor1.add(r);
                    }
                }
            
            floors.addChangeListener(new ChangeListener()
            {

                public void stateChanged(ChangeEvent e) {
                                JTabbedPane pane = (JTabbedPane) e.getSource();
                                currenttabbedpane = pane.getSelectedIndex();
                }
            });
            floors.setSelectedIndex(currenttabbedpane);

            panel1.add(backbuttonarea, BorderLayout.NORTH);
            panel1.add(floors, BorderLayout.CENTER);
            
            myframe.add(panel1);

            panel1.revalidate();
            panel1.repaint();
        }
        
        public int determinepriority(int priority){
            if (priority>0&& priority<300)
            {return 1;}
            else if(priority>=300 && priority<600)
            {return 2;}
            else if(priority>=600 && priority<900)
            {return 3;}
            else if(priority>=900 && priority<=1200)
            {return 4;}
            else if(priority>1200)
            {return 5;}
            else{
            return -1;
            }
            
        }
        
        
	public void viewlogs(){
		myframe.setSize(1000,550);
		myframe.remove(panel1);
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		myframe.add(panel1);
                
                JPanel panel2 = new JPanel(new FlowLayout());
                
                int timestampcolumnNumber =5; //CANNOT BE THE FIRST COLUMN IN THE LOG

	

		int numberOfColumns=0;
		ResultSetMetaData rsmd=null;

		try {
			myStmt=  myConn.createStatement();
			myRs = myStmt.executeQuery("select * from Events");
			rsmd = myRs.getMetaData();
			numberOfColumns = rsmd.getColumnCount()+1;

		}catch (Exception exc){System.out.println("Something went wrong");}

		String[] columnNames = new String[numberOfColumns];
		try {
		for(int i=0;i<numberOfColumns-1;i++){
			columnNames[i] = rsmd.getColumnName(i+1);
		}
                columnNames[columnNames.length - 1] = "Priority";

		}catch (Exception exc){System.out.println("Something went wrong");}


		int numberOfRows=0;
		
		try {
		while(myRs.next()){
			numberOfRows++;
		}

		}catch (Exception exc){System.out.println("Something went wrong");}

		Object[][] data= new Object[numberOfRows][numberOfColumns];
		try {
			int row = 0;
                        Date time_entered=null;
			myRs = myStmt.executeQuery("select * from Events");
			while (myRs.next()) {

			    for (int i = 0; i < numberOfColumns-1; i++) {
				data[row][i] = myRs.getObject(i+1);
                                if (i==timestampcolumnNumber-1){
                                    if(myRs.getTimestamp(i+1)!=null){
                                    //System.out.println(myRs.getTimestamp(i+1));
                                    Timestamp ts1 = myRs.getTimestamp(i+1);
                                    time_entered= new java.util.Date(ts1.getTime());
                                    }
                                    else{
                                    time_entered = null;//new Timestamp(0,0,0,0,0,0,0);
                                    //System.out.println(time_entered);
                                    //System.out.println("step1");
                                    }
                                }
                                
                                //time_entered=null;
                                    
                                //System.out.println(i);
			    }
                            //data[row][i+1]=
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    //get current date time with Date()
                    Date timenow = new Date();
                    //System.out.println(dateFormat.format(timenow));
                    long timediffinsec;
                    if (time_entered!=null){
                        timediffinsec = (timenow.getTime() - time_entered.getTime())/(1000);
                    }else{ timediffinsec=0;}
           /*
                            System.out.println(dateFormat.format(time_entered));
                            System.out.println(dateFormat.format(timenow));
                            System.out.println(timediffinsec);
                   */
                            //System.out.println("timediff:"+timediffinsec);
                            data[row][numberOfColumns-1]=timediffinsec;
			    row++;
			}
		}catch (Exception exc){System.out.println("Something went wrong in the populating of the 2d array data");}

		JTable table = new JTable(data, columnNames);


		table.setPreferredScrollableViewportSize(new Dimension(500,250));
		table.setFillsViewportHeight(true);
		JScrollPane scrollpane = new JScrollPane(table);
                
                
                JButton b1 = new JButton("Back");
                b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	stopautoupdate = true;
                                scheduler.schedule(new Runnable() {public void run() { ((ScheduledFuture<?>)(bph)).cancel(true); }},0, SECONDS);
                                menu();
			}
		});
                panel2.add(b1);
		panel1.add(scrollpane, BorderLayout.CENTER);
                panel1.add(panel2, BorderLayout.SOUTH);
                
		panel1.revalidate();
		panel1.repaint();
           
	}
        public void settings(){
            
                myframe.setSize(400,450);
		myframe.remove(panel1);
		panel1 = new JPanel();
		panel1.setLayout(new BorderLayout());
		myframe.add(panel1);
                
                JPanel toppanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                Box mybox = Box.createVerticalBox();
                
                JLabel label1 = new JLabel("Enter rooms to display(sepratated by commas):");
                
                
                JLabel label2 = new JLabel("Enter the desired periodic update in seconds");
                JTextField periodicupdate = new JTextField("10",3);
                JPanel periodicinfo = new JPanel(new FlowLayout(FlowLayout.LEFT));
                periodicinfo.add(label2);
                periodicinfo.add(periodicupdate);
                
                
                JTextArea ta1 = new JTextArea();
                ta1.setLineWrap (true);
                
                JPanel panelforbuttons = new JPanel(new FlowLayout());
                JButton apply = new JButton("Apply");
                JButton cancel = new JButton("cancel");
                panelforbuttons.add(apply);
                panelforbuttons.add(cancel);
                
                JCheckBox deleterooms = new JCheckBox("Delete Rooms");
                
                JButton backbutton = new JButton("Back");
                backbutton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	menu();
			}
		});
    
                cancel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                            	menu();
			}
		});
                
                apply.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
                                //System.out.println(Integer.parseInt(periodicupdate.getText())+1);
                                defaultperiodicupdate = Integer.parseInt(periodicupdate.getText());
                                String rooms = ta1.getText();
                            if(rooms.equalsIgnoreCase(""))   {
                            }
                            else{
                                LinkedList<Integer> listofrooms = new LinkedList<Integer>();
                                
                                int length = rooms.length();
                                
                                //System.out.println(length);
                                while(rooms.contains(",")){
                                    int i =0;
                                    //System.out.println(rooms);
                                    int locationofcomma = rooms.indexOf(',');
                                    int roomtoadd =Integer.parseInt(rooms.substring(i,locationofcomma));
                                    if(roomtoadd>100){
                                    listofrooms.add(roomtoadd);}
                                    i=locationofcomma+1;
                                    rooms=rooms.substring(i,length);
                                    length=rooms.length();      
                                }
                                int roomtoadd2= Integer.parseInt(rooms);
                                if(roomtoadd2>100){
                                listofrooms.add(Integer.parseInt(rooms));}
                                ListIterator iter1 = listofrooms.listIterator();
                                
                                if(deleterooms.isSelected()){
                                    try{
                                        PreparedStatement pstmt = myConn.prepareStatement("Delete from rooms where room_number >0");
                                        pstmt.executeUpdate();
                                    while(iter1.hasNext()){
                                        PreparedStatement pstmt2 = myConn.prepareStatement("insert into rooms(room_number, occupied) values(?,?)");
                                        pstmt2.setInt(1,(int)(iter1.next()));
                                        pstmt2.setInt(2,0);
                                        pstmt2.executeUpdate();
                                           // System.out.println(iter1.next());
                                    }
                                    
                                    }catch(Exception exc){System.out.println("Error while deleting rooms");}
                                }else{
                                    try{

                                    while(iter1.hasNext()){
                                        try{
                                        PreparedStatement pstmt2 = myConn.prepareStatement("insert into rooms(room_number, occupied) values(?,?)");
                                        pstmt2.setInt(1,(int)(iter1.next()));
                                        pstmt2.setInt(2,0);
                                        pstmt2.executeUpdate();
                                           // System.out.println(iter1.next());
                                        }catch(Exception exc){System.out.println("Could not insert a room");}
                                    }
                                    
                                    }catch(Exception exc){System.out.println("Error while inserting new rooms");}
                                }
                                
                            }
                            	menu();
			}
		});
                

                JPanel panelforbackbutton = new JPanel(new FlowLayout(FlowLayout.LEFT));
                JPanel panelforlabel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
                panelforbackbutton.add(backbutton);
                panelforlabel1.add(label1);
                panelforlabel1.add(deleterooms);
                mybox.add(panelforbackbutton);
                mybox.add(periodicinfo);
                mybox.add(panelforlabel1);
                
                toppanel.add(mybox);
                panel1.add(toppanel, BorderLayout.NORTH);
                panel1.add(ta1, BorderLayout.CENTER);
                panel1.add(panelforbuttons, BorderLayout.SOUTH);
                myframe.revalidate();
		myframe.repaint();
        }

}

